#+TITLE: Emacs Configuration
#+AUTHOR: Chase Dillard

* Personal Information
#+begin_src emacs-lisp
(setq user-full-name "Chase Dillard"
    user-mail-address "c.dillard@protonmail.com")
#+end_src

* My Functions
#+begin_src emacs-lisp
(defun my/edit-default-config ()
  "Edits main config file. Do not use to add custom config. Use 'my/edit-custom-config' instead."
  (interactive)
  (find-file (concat user-emacs-directory "config.org")))
  
(defun my/edit-custom-config ()
  "Edits custom config file. Useful for everything added to config."
  (interactive)
  (find-file (concat "~" ".chasemacs")))
#+end_src
* Customize Settings
#+begin_src emacs-lisp
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)
#+end_src

* Emacs General
** Projectile
#+begin_src emacs-lisp
(use-package projectile)
#+end_src

** Which Key
#+begin_src emacs-lisp
(use-package which-key
  :config
  (which-key-mode))
#+end_src

** Helm
#+begin_src emacs-lisp
(use-package helm
  :demand t
  :config
  (global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
  (global-set-key (kbd "C-x C-f") #'helm-find-files)
  (global-set-key (kbd "M-x") #'helm-M-x))

(use-package helm-swoop)
#+end_src
   
** Yasnippet
#+begin_src emacs-lisp
(use-package yasnippet
  :config
  (yas-load-directory (concat user-emacs-directory "snippets"))
  (yas-global-mode t))
  
(use-package yasnippet-snippets)
#+end_src

** Org
*** Org Mode
#+begin_src emacs-lisp
(use-package org)
#+end_src

*** Org Bullets
#+begin_src emacs-lisp
(use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda() (org-bullets-mode t))))
#+end_src
   
** Dash
#+begin_src emacs-lisp
(use-package dash)
#+end_src
   
** S & F
#+begin_src emacs-lisp
(use-package s)

(use-package f)
#+end_src

* Theme
** Theme Loading
#+begin_src emacs-lisp :tangle no
(load-theme 'aurora)
(setq sml/no-confirm-load-theme t)
#+end_src

** Theme Functions
#+begin_src emacs-lisp
(defun disable-active-themes ()
  (interactive)
  (mapc #'disable-theme custom-enabled-themes))

(defun switch-theme (theme)
  "Disables any currently active themes and loads THEME."
  (interactive
    (list
      (intern (completing-read "Load custom theme: "
                               (mapc 'symbol-name
			             (custom-available-themes))))))
  (let ((enabled-themes custom-enabled-themes))
    (mapc #'disable-theme custom-enabled-themes)
    (load-theme theme t)))
#+end_src

** Doom Theme
#+begin_src emacs-lisp
(use-package doom-themes)

(if (not window-system)
  (switch-theme 'doom-nord))
#+end_src

* Languages
** C#
#+begin_src emacs-lisp
(use-package csharp-mode
  :mode (("\\.cs\\'" . csharp-mode))
  :config
  (add-hook 'csharp-mode-hook #'company-mode))
#+end_src

** Rust
#+begin_src emacs-lisp
(use-package rust-mode
  :mode (("\\.rs\\'" . rust-mode))
  :config
  (add-hook 'rust-mode-hook #'company-mode))
#+end_src

** Lua
#+begin_src emacs-lisp
(use-package lua-mode
  :mode (("\\.lua\\'" . lua-mode))
  :config
  (add-hook 'lua-mode-hook #'company-mode))
#+end_src
   
** Ruby
#+begin_src emacs-lisp
(use-package ruby-mode
  :mode (("\\.rb\\'" . ruby-mode))
  :config
  (add-hook 'ruby-mode-hook #'company-mode))
#+end_src

** Markdown
#+begin_src emacs-lisp
(use-package markdown-mode
  :mode (("\\.md\\'" . markdown-mode))
  :config
  (add-hook 'markdown-mode-hook #'company-mode))
#+end_src

* Text Editing
** Parentheses
*** SmartParens
#+begin_src emacs-lisp
(use-package smartparens
  :config
  (sp-local-pair '(org-mode emacs-lisp-mode) "'" "'" :actions nil)
  (setq-default sp-escape-quotes-after-insert nil)
  (setq sp-highlight-pair-overlay nil)
    (progn
      (require 'smartparens-config)
      (smartparens-global-mode t)))
#+end_src
    
*** Rainbow Delimiters
#+begin_src emacs-lisp
(use-package rainbow-delimiters)
#+end_src
    
** Undo Tree
#+begin_src emacs-lisp
(use-package undo-tree)
#+end_src
   
** Multiple Cursors
#+begin_src emacs-lisp
(use-package multiple-cursors)
#+end_src

** Company
#+begin_src emacs-lisp
(use-package company)
#+end_src

** Expand Region
#+begin_src emacs-lisp
(use-package expand-region)
#+end_src

** Flycheck
#+begin_src emacs-lisp
(use-package flycheck)
#+end_src

* Version Control
** Git
#+begin_src emacs-lisp
(use-package magit)
#+end_src

* Evil Mode
#+begin_src emacs-lisp
(use-package evil-escape
  :config
  (evil-escape-mode t)
  (setq-default evil-escape-key-sequence "jk")
  (setq-default evil-escape-delay 0.5)
  (push 'visual evil-escape-excluded-states))

(use-package evil
  :demand t
  :config
  (setcdr evil-insert-state-map nil)
  (define-key evil-insert-state-map [escape] 'evil-normal-state)
  (define-key evil-normal-state-map (kbd "C-u") 'evil-scroll-up)
  (define-key evil-normal-state-map (kbd "[ m") 'beginning-of-defun)
  (define-key evil-normal-state-map (kbd "] m") 'end-of-defun)

  ; swap ; and :
  (define-key evil-motion-state-map (kbd ":") 'evil-repeat-find-char)
  (define-key evil-motion-state-map (kbd ";") 'evil-ex)

  (add-hook 'org-mode-hook
    (lambda ()
      (define-key evil-normal-state-map (kbd "TAB") 'org-cycle)))
    (evil-mode t)

  (use-package evil-leader
  :demand t
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    "fc" 'my/edit-custom-config
    "fm" 'my/edit-default-config
    "ff" 'helm-find-files)

  (global-evil-leader-mode))

  (use-package evil-surround
  :demand t
  :config
  (global-evil-surround-mode)))
#+end_src

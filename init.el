;;;; top of file
;;; package requirements
(when window-system
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1))

(require 'package)
(package-initialize)

(add-to-list 'package-archives '("melpa" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;;; themes
; (load-theme 'aurora)
; (setq sml/no-confirm-load-theme t)

;;; use-package initialization
(unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

(eval-when-compile
    (require 'use-package))
(setq use-package-always-ensure t)

;;; load config.org file
(org-babel-load-file (concat user-emacs-directory "config.org"))
;;;; end of file
